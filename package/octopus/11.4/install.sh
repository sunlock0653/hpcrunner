#!/bin/bash

##全局变量
#--------------------------------#
#having test
version=$1;

##全局变量
#--------------------------------#
passwd=123456;
install_path=/share/home/top_software/sunqian
target_path=$install_path/bin
source_file=$install_path/source_sun.sh
cpu_num=`grep -c "processor" /proc/cpuinfo`; #CPU核数
netcard=`cat /proc/net/dev|sort -rn -k2|awk '{print $1}'|awk NR==1|awk -F ':' '{print $1}'`;#网卡名称
ip_addr=`ip a|grep $netcard|grep inet|awk '{print $2}'|awk -F '/' '{print $1}'` #ip地址
osType=`cat /etc/os-release|grep ID |awk NR==1|awk -F \" '{print $2}'`
basearch=`arch`
#--------------------------------#
echo install_path=/share/home/top_software/sunqian >> $source_file
touch $source_file
#--------------------------------#
#下载全部所需的依赖
cd $install_path
wget https://ftp.gnu.org/gnu/gsl/gsl-2.5.tar.gz
wget https://launchpad.net/libvdwxc/stable/0.4.0/+download/libvdwxc-0.4.0.tar.gz
wget https://github.com/Reference-LAPACK/lapack/archive/refs/tags/v3.10.1.tar.gz
wget http://forge.abinit.org/fallbacks/libxc-4.0.1.tar.gz
wget http://www.fftw.org/fftw-3.3.8.tar.gz
wget https://octopus-code.org/download/11.4/octopus-11.4.tar.gz
wget https://github.com/kunpengcompute/xucg/archive/refs/tags/v1.2.0-huawei.tar.gz




#安装libxc 4.0.1
cd $install_path
tar -xvf libxc-4.0.1.tar.gz
cd libxc-4.0.1
CC=gcc
FC="gfortran -fallow-argument-mismatch"
./configure --prefix=$target_path/libxc --enable-shared=yes --enable-static=yes
make -j$(nproc)
make install


cat>"$target_path/libxc/libxc_modulefiles"<<EOF
#%Module1.0
conflict libxc
variable modfile [file normalize [info script]]
proc getModulefileDir {} {
    variable modfile
    set modfile_path [file dirname \$modfile]
    return \$modfile_path
}
set pwd [getModulefileDir]
set LIBXC \$pwd
setenv LIBXC \$LIBXC
prepend-path PATH				\$LIBXC/bin
prepend-path LD_LIBRARY_PATH \$LIBXC/lib
prepend-path INCLUDE			\$LIBXC/include
EOF
module use $target_path/libxc/
module load $target_path/libxc/libxc_modulefiles

#安装fftw
cd $install_path
tar -xvf fftw-3.3.8.tar.gz
cd fftw-3.3.8
./configure CC=gcc MPICC=mpicc F77="gfortran -fallow-argument-mismatch"  --enable-shared --enable-threads --enable-openmp --enable-mpi --enable-fma --enable-neon MPICC=mpicc --prefix=$target_path/fftw
make -j$(nproc)
make install

#安装lapack
cd $install_path
tar -xvf lapack-3.10.1.tar.gz 
cd lapack-3.10.1
cp make.inc.example make.inc
sed –i -e '/^CC/s+gcc+clang+g' -e '/^FC/s+gfortran+flang+g' -e '/^TIMER = INT_ETIME/s+^+#+g' -e '/^#TIMER = INT_CPU_TIME/s+#++g' -e 's/librefblas.a/libblas.a/g' -e 's/-frecursive//g' make.inc
sed -i 's/lib: lapacklib tmglib/lib: blaslib variants lapacklib tmglib cblaslib blaspplib/g' Makefile

make -j$(nproc)
mkdir -p $target_path/lapack/lib
cp lib*.a $target_path/lapack/lib/


#安装libvdwxc
cd $install_path
tar -xvf libvdwxc-0.4.0.tar.gz
cd libvdwxc-0.4.0
./configure MPICC=mpicc MPIF90="mpifort -fallow-argument-mismatch" MPIFC=mpifort CC=mpicc CXX=mpic++ FC=mpifort --prefix=$target_path/libvdwxc --with-fftw3=$target_path/fftw

make -j $(nproc)
make install

#4.6 安装gsl
cd $install_path
tar -xvf gsl-2.5.tar.gz
cd gsl-2.5
./configure CC=clang --prefix=$target_path/gsl
make -j$(nproc)
make install

#octopus
cd $install_path
mkdir –p $target_path/octopus

tar -xvf octopus-11.4.tar.gz

cd octopus-11.4
#/usr/bin/ld: /usr/lib/x86_64-linux-gnu/lapack/liblapack.a(xerbla.o): undefined reference to symbol '_gfortran_string_len_trim@@GFORTRAN_8'
#/usr/bin/ld: /lib/x86_64-linux-gnu/libgfortran.so.5: error adding symbols: DSO missing from command line
#为解决上述两条报错加入-lgfortran
./configure CC="mpicc -lgfortran" CXX=mpicxx FC="mpifort -lgfortran" --prefix=$target_path/octopus/octopus-11.4 \
--with-libxc-prefix=$target_path/libxc --with-libvdwxc-prefix=$target_path/libvdwxc --with-blas=$target_path/lapack/lib/librefblas.a \
--with-lapack=$target_path/lapack/lib/liblapack.a --with-gsl-prefix=$target_path/gsl --with-fftw-prefix=$target_path/fftw \
--enable-mpi="-L/share/home/mpi2/bin/hmpi/lib/"  --disable-zdotc-test



sed -i -e '0,/FINISHED/{s/FINISHED/FINISHED1/}' -e 's/(FINISHED/(FINISHED1/g' src/multisystem/propagator.F90
sed -i 's/type(\*)/type(c_ptr)/g' src/basic/cuda.F90
sed -i '0,/private/{s/private/\!private/}' src/basic/clock.F90
sed -i 's/FINISHED/FINISHED1/g' src/multisystem/system.F90
sed -i -e 's/test_clock_a .lt. test_clock_b/clock_is_earlier(test_clock_a,test_clock_b)/g' -e 's/test_clock_a .le. test_clock_b/clock_is_equal_or_earlier(test_clock_a,test_clock_b)/g' -e 's/test_clock_a .gt. test_clock_b/clock_is_later(test_clock_a,test_clock_b)/g' -e 's/test_clock_a .ge. test_clock_b/clock_is_equal_or_later(test_clock_a,test_clock_b)/g' -e 's/test_clock_a .eq. test_clock_b/clock_is_equal(test_clock_a,test_clock_b)/g' src/main/test.F90
sed -i '/SAFE_DEALLOCATE_P(systems)/s+^+!+g' src/main/run.F90
sed -i -e 's/cuda_memcpy_htod(this%mem, data(1)/cuda_memcpy_htod(this%mem, c_loc(data(1))/g' -e 's/cuda_memcpy_htod(this%mem, data(1, 1)/cuda_memcpy_htod(this%mem, c_loc(data(1, 1))/g' -e 's/cuda_memcpy_htod(this%mem, data(1, 1, 1)/cuda_memcpy_htod(this%mem, c_loc(data(1, 1, 1))/g' -e 's/cuda_memcpy_dtoh(this%mem, data(1)/cuda_memcpy_dtoh(this%mem, c_loc(data(1))/g' -e 's/cuda_memcpy_dtoh(this%mem, data(1, 1)/cuda_memcpy_dtoh(this%mem, c_loc(data(1, 1))/g' -e 's/cuda_memcpy_dtoh(this%mem, data(1, 1, 1)/cuda_memcpy_dtoh(this%mem, c_loc(data(1, 1, 1))/g' src/basic/accel_inc.F90

make -j$(nproc)
make install

#测试
cat>"$target_path/octopus/octopus-11.4/octopus_modulefiles"<<EOF
#%Module1.0
conflict octopus
variable modfile [file normalize [info script]]
proc getModulefileDir {} {
    variable modfile
    set modfile_path [file dirname \$modfile]
    return \$modfile_path
}
set pwd [getModulefileDir]
set OCTOPUS \$pwd
setenv OCTOPUS \$OCTOPUS
prepend-path PATH \$OCTOPUS/bin
prepend-path LD_LIBRARY_PATH \$OCTOPUS/lib
EOF

module use $target_path/octopus/octopus-11.4/
module load $target_path/octopus/octopus-11.4/octopus_modulefiles


cd $install_path/octopus-11.4/testsuite/finite_systems_3d
cp 23-go-na2.03-fire.inp inp
mpirun --allow-run-as-root -np 128 -x OMP_NUM_THREADS=1 octopus